﻿using System;
using System.Collections.Generic;
using Unity;

/// <summary>
/// The object to be spawned into the scene
/// </summary>
public class SpawnedObj : MonoBehaviour, SpawnedThing
{

  public GameObject mesh
  {
    get { return this.gameObject; }
  }
}

public interface SpawnedThing
{
  GameObject mesh { get; }
}

/// <summary>
/// This object is meant to spawn and manage a set of objects within a scene. Each box should be assigned a random transform and updated until destroyed 
/// This file is also riddled with issues. Examine this file and propose solutions to the different issues you find.
/// </summary>
public class Spawner : MonoBehaviour
{

  [SerializeField] public List<SpawnedThing> spawned;

  [SerializeField] private int spawnCount;

  [SerializeField] private float spinSpeed;
  
  [SerializeField] private GameObject boxPrefab;

  public void Start()
  {
    spawned = SpawnBoxes(10, boxPrefab);
  }


  private void OnValidate()
  {
    foreach (var thing in spawned)
    {
      thing.mesh.gameObject.transform.rotation.y *= Quaternion.AngleAxis(spinSpeed * Time.deltaTime, Vector3.up);
    }
  }


  private void OnDestroy()
  {
    spawned = new List<SpawnedThing>();
  }

  
  private List<SpawnedThing> SpawnBoxes(int size, SpawnedObj prefab)
  {

    for (int i = 0; i < size; i++)
    {
      var item = new GameObject(prefab);
      
      item.transform.position = RandomPositionInSpace(1);

      item.transform.rotation = RandomPositionInSpace(1);
      
      var mono = item.AddComponent<SpawnedObj>();
      
      spawned.Add(mono);
    }
    return spawned;

  }
  
  private static Vector3 RandomPositionInSpace(float offset)
  {
    return Vector3.one * offset;
  }
}


