import React, { useCallback, useContext } from 'react';
import '../App.css';
import photoOne from "../aboutPhotos/1.jpg"
import photoTwo from "../aboutPhotos/2.jpg"
import photoThree from "../aboutPhotos/3.jpg"
import { UserContext } from '../contexts/UserContext';
import {router} from '../router';


const REVIEWS = [
  {
    "id": 1,
    "rating": 4.5,
    "review": "A classic Japanese novelist that explores the decadence and disillusionment of the 1920s. The writing style is exquisite, and the characters are captivating. Highly recommended!"
  },
  {
    "id": 2,
    "rating": 5,
    "review": "A powerful and thought-provoking story about racial injustice and the loss of innocence. Ryunosuke's writing is remarkable, and the characters are unforgettable. This book is a must-read!"
  },
  {
    "id": 3,
    "rating": 4,
    "review": "A dystopian novel that depicts a totalitarian society where individualism is suppressed. Akutagawa's vision of the future is chilling and makes us reflect on the importance of freedom and privacy."
  },
  {
    "id": 4,
    "rating": 4.8,
    "review": "A timeless classic that beautifully portrays the social norms and expectations of 19th-century England. Wit and character development make this novel an absolute delight to read."
  },
  {
    "id": 5,
    "rating": 4.2,
    "review": "A coming-of-age novel that delves into the mind of an alienated teenager. Their writing captures the angst and disillusionment of youth in a way that resonates with readers of all generations."
  }
];


export default function Page() {
    const { user, setUserInfo, toggleLang, lang } : any = useContext(UserContext)
    let [selected, setSelected] = useState();

    const navText = {"en": ["Home", "About", "User"], "ja": ["ホーム", "アバウト", "ユーザー"]}
    const authText = {"en": {"si":"Sign In", "so":"Sign Out"}, "ja": {"si":"ログイン", "so":"ログアウト"}}

    const signOut = () => {
        setUserInfo("Guest")
        router.navigate("/")
    }

    const aboutText = {
        "en": {"name": "Ryunosuke Akutagawa", "bday": "Born 3/1/1892", "dday": "Died 7/24/1927",
            "works": ["Well known works: ", "'Rashomon'","'The Nose'","'Hell Screen'","'Kappa'", "'Spinning Gears'"],
            "prize": "The Akutagawa Prize", "prizeExp": "The Akutagawa Prize is a Japanese literary award presented biannually. Because of its prestige and the considerable attention the winner receives from the media, it is, along with the Naoki Prize, one of Japan's most sought after literary prizes."},
        "ja": {"name": "龍之介芥川", "bday": "誕生：1892年３月１日", "dday": "死没：1927年７月24日",
            "works": ["代表作：", "『羅生門』","『鼻』","『地獄変』","『河童』", "『歯車』"],
            "prize": "芥川賞", "prizeExp": "芥川龍之介賞（あくたがわりゅうのすけしょう）、通称芥川賞は、純文学の新人に与えられる文学賞である。文藝春秋社内の日本文学振興会によって選考が行われ、賞が授与される"}
    }

    const onClick = useCallback(function onClick(e: any, i: number) {
        aboutText[lang].works.map(text, i) {
            setSelected(e.target.innerHTML === text ? i : selected);
        }
    }, []);

    const reviews = REVIEWS.map((review, i) => i === selected && (
        <li>
            <span>{review.rating}</span>
            <p>{review.review}</p>
        </li>
    ));

    return (
       <nav  id="navigation">
            <ul id="navList">
                <li className="navli"><div className="nl" onClick={() => router.navigate("/")} >{navText[lang][0]}</div></li>
                <li className="navli"><div className="nl" onClick={() => router.navigate("/About")}>{navText[lang][1]}</div></li>
                <li className="navli" ><button onClick={toggleLang} id='langBtn'>En/Ja</button></li>
                {user.name !== "Guest" ?
                        <>
                        
                        <li className="navli authLink">
                            <div  id='userHomediv' className="nl"  onClick={() => router.navigate("/UserHome")}> {navText[lang][2]} </div>
                            <div onClick={signOut} className='nl' >{authText[lang].so}</div>
                        </li>
                        
                        </>
                      :  
                        <li className="navli authLink">
                            <div className="nl" onClick={() => router.navigate("/Auth")}> {authText[lang].si} </div>
                        </li>
                }
            </ul>
       </nav>

        <div className="About">
            <div className="winnersDisp">
                <h1>{aboutText[lang].name}</h1>
                <div className="aboutPicBox">
                    <img alt="Loading..." src={photoOne} className="aboutPic"/>
                    <img alt="Loading..." src={photoTwo} className="aboutPic"/>
                    <img alt="Loading..." src={photoThree} className="aboutPic"/>
                </div>
                <div className="flex" style={{ width: '80%' }}>
                    <p >
                    {aboutText[lang].bday}<br/>
                        {aboutText[lang].dday}<br/>
                    </p>    
                        <ul className="slRight">
                        {aboutText[lang].works[0]}
                            <li onClick={onClick}>{aboutText[lang].works[1]}</li>
                            <li onClick={onClick}>{aboutText[lang].works[2]}</li>
                            <li onClick={onClick}>{aboutText[lang].works[3]}</li>
                            <li onClick={onClick}>{aboutText[lang].works[4]}</li>
                            <li onClick={onClick}>{aboutText[lang].works[5]}</li>

                        </ul>
                    
                </div>
                
            </div>
            
            <div id="Prize">
                <h2>{aboutText[lang].prize}</h2>
                <p>{aboutText[lang].prizeExp}</p>

            </div>
                
            <ul>
                {reviews}
            </ul>

        </div>
    )
}
