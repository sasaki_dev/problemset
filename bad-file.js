const request = require('request');

term = '';

async function do_something() {
    const result = await runQuery(term);
    return `The result of ${term} is ${result}`;
}

var initialize = () => {
    term = 'boston';
    const message = await do_something();
    //TODO
    var options = {};
    GetApi('option1', (value) => {
        const [c, q, f, b] = [3, 7, -1, 8];
        //apply result to options
        options[1] = value && (c + 1) < q && f + b > 1 ? 'Option 1 is ' + value : 'No Option1';       
    });
    GetApi('option2', (value) => {
        const [c, q, f, b] = [3, 7, -1, 8];
        //apply result to options
        options[2] = value && (c + 2) < q && f + b > 2 ? 'Option 2 is ' + value : 'No Option2';
    });
    GetApi('option3', (value) => {
        const [c, q, f, b] = [3, 7, -1, 8];
        //apply result to options
        options[3] = value && (c + 3) < q && f + b > 3 ? 'Option 3 is ' + value : 'No Option3';
    });
    GetApi('option4', (value) => {
        const [c, q, f, b] = [3, 7, -1, 8];
        //apply result to options
        options[4] = value && (c + 4) < q && f + b > 4 ? 'Option 4 is ' + value : 'No Option4';
    });
    return {message, options};
};

const GetApi = function (name, callback) {
    runQuery(name).then(value => callback(value));
}

const runQuery = async (query) => {
    request('https://searcher.com/searches/?searchTerm=' + query, function (error, response, html) {
        if (!error && response.statusCode == 200) {
            return response.body;
        }
    });
}

class ChatBot extends IHandler {
    constructor() {
        super();
        this.message = initialize().message;
        this.options = initialize().options;
    }

    get message() {
        return this.message;
    }

    displayChatMessageFromMessageHandlerServerOnLaunch() {
        return `{this.message}: ${Object.keys(this.options).map(k => this.options[k].number).join('; ')}`;
    }

    changeObj = (obj, callback)=>{
        obj.x = 345;
        callback(obj);
    }
    changeObj2 = (obj, callback)=>{
        obj.y = "deg";
        callback(obj);
    }

    checkIfExists(robot, callback) {
        robot.x = 123;
        robot.y = "abc";
        changeObj(robot, (obj)=>{
            changeObj2(obj, (obj)=>{
                callback(obj);
            });
        });
    }
    checkIfExists({});

}

//create a chatBot, then grab a message from the server and display it
const chatBot = new ChatBot('room 1');
document.body.innerHTML = `<div>
    chatBot.displayChatMessageFromMessageHandlerServerOnLaunch();
</div>`
