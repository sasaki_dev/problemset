# General issues

## Things to look for

* Issues with code clarity
    * Mixing different JS styles / approaches
    * Unclear code lacking comments
    * Unclear comments
* Issues with code functionality
    * Efficiency issues
    * Execution order (e.g. async)
* Best practice violations
    * Needlessly repeating code
    * Unused methods, variables or parameters
    * Naming things inconsistently
